package br.com.tfs.animal;

import br.com.tfs.util.InformacoesLogin;
import br.com.tfs.util.OperacoesHTTP;
import br.com.tfs.util.URLS;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String Email;
	private String Senha;

	// UI references.
	private EditText etEmail;
	private EditText etSenha;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private Context contexto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// Set up the login form.
		//mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		etEmail = (EditText) findViewById(R.id.etEmail);
		//mEmailView.setText(mEmail);
		contexto = this; 
		etSenha = (EditText) findViewById(R.id.etSenha);
		etSenha.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.btnLogin).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		etEmail.setError(null);
		etSenha.setError(null);

		// Store values at the time of the login attempt.
		Email = etEmail.getText().toString();
		Senha = etSenha.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(Senha)) {
			etSenha.setError(getString(R.string.action_esqueci_senha));
			focusView = etSenha;
			cancel = true;
		} else if (Senha.length() < 6) {
			etSenha.setError(getString(R.string.erro_campo_obrigatorio));
			focusView = etSenha;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(Email)) {
			etEmail.setError(getString(R.string.erro_campo_obrigatorio));
			focusView = etEmail;
			cancel = true;
		} else if (!Email.contains("@")) {
			etEmail.setError(getString(R.string.erro_email_invalido));
			focusView = etEmail;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progresso_logando);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected String[] doInBackground(Void... params) {
			try {
				// Simulate network access.
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return null;
			}
			InformacoesLogin.setEmail(Email);
			InformacoesLogin.setSenha(Senha);
			
			return OperacoesHTTP.GET(URLS.Login);
		}
		@Override
		protected void onPostExecute(String[] resultado) {
			mAuthTask = null;
			showProgress(false);
			Integer codigoHTTP = Integer.parseInt(resultado[1]);
			
			if(codigoHTTP == 401){
				//Mensagem que usu�rio ou senha est� errado ou usu�rio n�o cadastrado
				etSenha.setError(getString(R.string.erro_emailousenha_incorreta));
				etSenha.requestFocus();
			}else if (codigoHTTP == 403) {
				//Mensagem que n�o tem autoriza��o para acessar essa funcionalidade
				Toast.makeText(getBaseContext(), "N�o tem autoriza��o!", Toast.LENGTH_LONG).show();
			}else if(codigoHTTP == 500){
				Toast.makeText(getBaseContext(), "Erro do servidor!", Toast.LENGTH_LONG).show();
			}else if(resultado[0].equals("FALSE")){
				Toast.makeText(getBaseContext(), "Erro com conex�o!", Toast.LENGTH_LONG).show();
			}else {
				Intent inicialLogado = new Intent(contexto, CadastrarActivity.class);
				startActivity(inicialLogado);
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
