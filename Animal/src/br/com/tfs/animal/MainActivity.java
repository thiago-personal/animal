package br.com.tfs.animal;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	Button btnLogin;
	Button btnCadastrar;
    Context contexto;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        btnLogin = (Button)findViewById(R.id.main_btnLogin);
        btnCadastrar = (Button)findViewById(R.id.main_btnCadastrar);
        contexto = this;
        
        btnLogin.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {  
        		Intent login = new Intent(contexto, LoginActivity.class);
        		startActivity(login);
        	}
        });
        
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent cadastrar = new Intent(contexto, CadastrarActivity.class);
				startActivity(cadastrar);
			}
		});
        
        //etResposta = (EditText) findViewById(R.id.etResposta);
       // conectado = (TextView) findViewById(R.id.conectado);
        
        //new HttpAsyncTask().execute("http://hahaha.somee.com/api/Estado/Get");
        //new HttpAsyncTask().execute("http://hahaha.somee.com/api/Usuario/Login");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
 
   /* public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) 
                return true;
            else
                return false;   
    }
    
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return OperacoesHTTP.GET(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();
            etResposta.setText(result);
       }
    }*/
}
