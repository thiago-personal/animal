package br.com.tfs.models;

public class CadastrarUsuarioComum {
    private Usuario CadastroDoUsuario = null;
    private UsuarioComum UsuarioComum = null;
    private CadastrarSenha Senha = null;
    
    public CadastrarUsuarioComum(Usuario cadastroDoUsuario, UsuarioComum usuarioComum, CadastrarSenha senha){
    	this.setCadastroDoUsuario(cadastroDoUsuario);
    	this.setUsuarioComum(usuarioComum);
    	this.setSenha(senha);
    }

	public CadastrarSenha getSenha() {
		return Senha;
	}

	public void setSenha(CadastrarSenha senha) {
		Senha = senha;
	}

	public UsuarioComum getUsuarioComum() {
		return UsuarioComum;
	}

	public void setUsuarioComum(UsuarioComum usuarioComum) {
		UsuarioComum = usuarioComum;
	}

	public Usuario getCadastroDoUsuario() {
		return CadastroDoUsuario;
	}

	public void setCadastroDoUsuario(Usuario cadastroDoUsuario) {
		CadastroDoUsuario = cadastroDoUsuario;
	}
}
