package br.com.tfs.models;

public class CadastrarSenha {
    private String Senha = null;
    private String ConfirmarSenha = null;
    
    public CadastrarSenha(String senha, String confirmarSenha){
    	this.setSenha(senha);
    	this.setConfirmarSenha(confirmarSenha);
    }
    
	public String getSenha() {
		return Senha;
	}
	public void setSenha(String senha) {
		Senha = senha;
	}
	public String getConfirmarSenha() {
		return ConfirmarSenha;
	}
	public void setConfirmarSenha(String confirmarSenha) {
		ConfirmarSenha = confirmarSenha;
	}
}
