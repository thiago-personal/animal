package br.com.tfs.models;

public class UsuarioComum {
    private String Sobrenome = null;
    private int Sexo = -1;
    private Usuario usuario = null;
    
    public UsuarioComum(String sobrenome, int sexo, Usuario usuario){
    	this.setSobrenome(sobrenome);
    	this.setSexo(sexo);
    	this.setUsuario(usuario);
    }
    
    public UsuarioComum(){
    }
    
	public String getSobrenome() {
		return Sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		Sobrenome = sobrenome;
	}
	public int getSexo() {
		return Sexo;
	}
	public void setSexo(int sexo) {
		Sexo = sexo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
